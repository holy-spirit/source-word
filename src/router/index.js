import VueRouter from "vue-router"
import Vue from "vue"
import Home from "@/view/Home.vue"
import Xuexi from "@/view/Xuexi/Xuexi.vue"
import Shezhi from "@/view/Shezhi/Shezhi.vue"
import Gywm from "@/view/Shezhi/neirong/gywm.vue"
import Sys from "@/view/Shezhi/neirong/sys.vue"
import Kxss from "@/view/Xuexi//kaishixuexi.vue"
import Yjfk from "@/view/Shezhi/neirong/yjfk.vue"
import Danciben from "@/view/Danciben//Danciben.vue"
import Dcb from "@/view/Danciben/dcb.vue"
import Dcxq from "@/components/dcxq.vue"
import Wdsc from "@/view/Shezhi/neirong/wdsc.vue"
import Wode from "@/view/Wode/Wode.vue"
import Shousuo from "@/view/Shousuo/Shousuo.vue"
Vue.use(VueRouter)
const routes = [
  { path: '/', redirect: '/home' },
  {
    path: '/home',
    component: Home,
    redirect: '/home/xuexi',

    children: [
      { path: 'xuexi', component: Xuexi, meta: { keepAlive: true } },
      { path: 'shezhi', component: Shezhi },
      { path: '/shezhi/gywm', component: Gywm },
      { path: '/shezhi/sys', component: Sys },
      { path: '/shezhi/yjfk', component: Yjfk },
      { path: 'ksxx', component: Kxss, },
      { path: 'danciben', component: Danciben },
      { path: '/danciben/:id', component: Dcb, },
      { path: '/dcxq', component: Dcxq },
      { path: '/shezhi/wdsc', component: Wdsc },
      { path: 'shousuo', component: Shousuo },
    ]
  },
  {
    path: '/wode',
    component: Wode
  }
]
export const router = new VueRouter({
  routes
})