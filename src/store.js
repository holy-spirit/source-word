import Vue from 'vue'
import Vuex from 'vuex'
// 该包能使Vuex的数据进行本地储存 在new Vuex.Store末尾加上plugins: [createPersistedState()]
import createPersistedState from "vuex-persistedstate"


Vue.use(Vuex)
export default new Vuex.Store({
  // 储存数据
  state: {
    // 存储进入单词本的数据
    dcbsj: [],
    // 单词收藏的默认选中(尚未完成)
    active: 0,
    // 离线版,每当新建单词本或者单词时，id为这个值，每新建一次，id+1(后续新增迁出迁入单词功能时，数据导出导入可能会发生id错误的BUG)
    id: 666,
    // 离线单词本重构
    lixian: [
      // html
      {      //dcbshouchang单词本是否收藏
        id: 1, name: 'html', dcbshouchang: false, data: [
          {
            id: 1,
            // 单词
            danci: "form",
            // 英标
            yinbiao: "bækɡraʊnd",
            // 单词翻译
            dcfy: "表单",
            // 释义
            shiyi: `表单用于收集用户的输入信息，配合不同的input输入框使用`,
            // 是否收藏
            shouchang: false
          },

        ]
      },
      // css
      {
        id: 2, name: 'css', dcbshouchang: false,
        data: [
          {
            id: 1,
            // 单词
            danci: "css",
            // 英标
            yinbiao: "bækɡraʊnd",
            // 单词翻译
            dcfy: "测试完成，请点击js查看具体功能",
            // 释义
            shiyi: `测试`,
            // 是否收藏
            shouchang: false
          },
        ]

      },
      // js
      {
        id: 3, name: 'JS', dcbshouchang: false,
        data: [{
          id: 1,
          // 单词
          danci: "js",
          // 英标
          yinbiao: "bækɡraʊnd",
          // 单词翻译
          dcfy: "自己填充吧",
          // 释义
          shiyi: `自己填充吧`,
          // 是否收藏
          shouchang: false
        },]
      },
     



    ],
    // 开始学习数据
    ksxxsj: [],
    // 学习计划数据
    hqjs: [
      {
        id: 1,
        cydc: "html", //常用单词标签
        yxdc: 13, //已学单词
        zgdc: 173, //总共单词
        jryx: 1, //今日已学
        ljts: 113, //累计天数
        img: "https://img01.yzcdn.cn/vant/cat.jpeg",//背景图
      },
      {
        id: 2,
        cydc: "css", //常用单词标签
        yxdc: 213, //已学单词
        zgdc: 213, //总共单词
        jryx: 21, //今日已学
        ljts: 2113, //累计天数
        img: "https://img01.yzcdn.cn/vant/cat.jpeg",
      },
      {
        id: 3,
        cydc: "js", //常用单词标签
        yxdc: 3, //已学单词
        zgdc: 373, //总共单词
        jryx: 3, //今日已学
        ljts: 33, //累计天数
        img: "https://img01.yzcdn.cn/vant/cat.jpeg",
      },
     

    ],
    //  我的收藏数据
    wdsc: {


      // 收藏单词本
      dcb: [],
      // 收藏单词
      dc: []
    },
    // 搜索数据
    dcgs: [],

  },
  // 修改state的数据
  mutations: {
    // (没有服务器，暂时弃用)把服务器获取的数据存储到state中
    /*  csj(state, sj) {
       console.log(sj, 'sj')
       state.dcbsj = sj
     }, */
    csj2(state, id) {
      // 通过id获取单词数据
      let shujv = state.lixian.find(function (obj) { return obj.id === id })
      state.dcbsj = shujv
      // 把处理好的数据放到浏览器的会话储存中，以免浏览器开发时刷新页面报错（利用该数据实现收藏功能）

      sessionStorage.setItem('dcbsj', JSON.stringify(shujv))
      console.log(state.dcbsj, 'state.dcbsj')
    },
    // 切换分页栏默认选中
    qh(state, title) {
      state.active = title
      console.log(state.active, 213213213)
    },
    // 接收开始学习的id,动态改变ksxxsj数组（这里需要优化filter）
    ksssa(state, id) {
      state.ksxxsj = []
      console.log(id)
      let a = state.lixian.filter(e => e.id == id)
      console.log(a[0].data, 'a[0].data')
      state.ksxxsj = a[0].data
      console.log(state.ksxxsj, 'state.ksxxsj')
    },


    // 点击单词本是否收藏（已优化）
    // data的数据，第一个为单词数据，第二个为单词本ID
    scdanci(state, data) {
      //  获取对应的索引 
      let index = state.lixian.findIndex(item => item.id == data[1])
      let index2 = state.lixian[index].data.findIndex(item => item.id == data[0].id)
      if (data[0].shouchang) {
        // 如果收藏,收藏单词数据添加该数据
        state.wdsc.dc.push(data[0])
      } else {
        // 如果不收藏,就删除收藏单词数据的对应单词
        //  获取对应的索引 
        let index3 = state.wdsc.dc.findIndex(item => item.id == data[0].id)
        console.log(index3, 'index3')
        state.wdsc.dc.splice(index3, 1)
      }
      //更新单词收藏状态
      state.lixian[index].data[index2].shouchang = data[0].shouchang
    },
    // 点击单词本是否收藏
    dancibensc(state, id) {

      state.lixian.forEach((item, index) => {
        if (item.id == id) {
          console.log(item, 'dcbscitem')
          state.lixian[index].dcbshouchang = !state.lixian[index].dcbshouchang
          console.log(typeof (state.lixian[index].dcbshouchang), '判断')
          console.log(state.lixian[index].dcbshouchang, 'dcbshouchang')
          // 如果单词本是收藏状态，则把单词本添加到收藏单词本中，反之删除
          if (state.lixian[index].dcbshouchang) {
            console.log(index, 'index')
            state.wdsc.dcb.push(state.lixian[index])
          } else {
            state.wdsc.dcb = state.wdsc.dcb.filter(val => val.id != id)
            console.log(state.wdsc.dcb, '单词本')
          }
        }
      })

    },
    // 添加单词本
    tianjiadcb(state, data) {

      let shujv = {      //dcbshouchang单词本是否收藏
        id: state.id, name: `${data.mz}`, dcbshouchang: JSON.parse(data.radio), data: []
      }
      let xuexijihua = {
        id: state.id,
        cydc: `${data.mz}`, //常用单词标签
        yxdc: 0, //已学单词
        zgdc: 0, //总共单词
        jryx: 0, //今日已学
        ljts: 0, //累计天数
        img: "https://img01.yzcdn.cn/vant/cat.jpeg",
      }
      state.hqjs.push(xuexijihua)
      state.lixian.push(shujv)
      //  如果选中收藏单词本，则收藏单词本也添加数据(用JSON.parse把字符串的false和true转换成布尔值)
      if (JSON.parse(data.radio)) {
        // state.wdsc.dcb.push(state.lixian[state.id - 1])
        state.lixian.forEach((item, index) => {
          if (item.id == state.id) {
            state.wdsc.dcb.push(item)
          }
        })

      }
      state.id += 1
      console.log(state.id, data, '添加单词本方法')
    },
    // 删除单词本
    shanchudcb(state, id) {
      let num = state.lixian.findIndex(item => item.id == id)
      //  如果该单词本为收藏，把收藏单词本的数据也删除
      if (state.lixian[num].dcbshouchang) {
        state.wdsc.dcb.splice(state.wdsc.dcb.findIndex(item => item.id == id), 1)
      }
      state.lixian.splice(num, 1)
      state.hqjs.splice(state.hqjs.findIndex(item => item.id == id), 1)
      /*  if (state.lixian[num].dcbshouchang) {
         console.log(state.lixian[num].dcbshouchang,'删除已收藏单词')
       } */

      console.log(state.lixian, '删除单词本')
    },
    // 添加单词
    tianjiadc(state, data) {
      console.log(data, 'data')
      // 处理数据
      let shujv = {
        id: state.id,
        // 单词
        danci: data[1].mz,
        // 英标
        yinbiao: "bækɡraʊnd",
        // 单词翻译
        dcfy: data[1].sy,
        // 释义
        shiyi: `${data[1].message}`,
        // 是否收藏
        shouchang: JSON.parse(data[1].radio)
      }
      console.log(data, '单词')
      // 添加
      state.lixian.filter((item, index) => {
        if (item.id == data[0]) {
          state.lixian[index].data.push(shujv)
        }
      })
      // 收藏单词添加
      if (shujv.shouchang) {
        state.wdsc.dc.push(shujv)
        console.log(state.wdsc.dc, '已收藏单词')
      }
      console.log(shujv, '添加单词数据')

      state.id += 1
    },
    // 删除单词
    shanchudc(state, data) {
      let index = state.lixian.findIndex(item => item.id == data[1])
      let index2 = state.lixian[index].data.findIndex(item => item.id == data[0].id)
      //  如果收藏了,把收藏的数据也删掉
      if (data[0].shouchang) {
        let index3 = state.wdsc.dc.findIndex(item => item.id == data[0].id)
        state.wdsc.dc.splice(index3, 1)
      }
      state.lixian[index].data.splice(index2, 1)
      console.log(state.lixian[index].data[index2], 'store删除单词')
    },
    // 搜索（离线1版正式完成）
    sousuo(state, data) {

      // dcgs
      if (data.length < 1) return state.dcgs = []
      state.dcgs = []
      console.log(data, '搜索')
      let shujv = []
      // 把所有单词都放到shujv里
      state.lixian.forEach(item => {
        item.data.forEach(item2 => {
          shujv.push(item2)
        })
      })
      shujv.map(item => {
        if (item.danci.search(data) != -1) {
          state.dcgs.push(item)
        }
      })

      console.log(shujv, '所有单词')
    },
    // 把点击单词的对应的单词本id放到会话储存，供搜索使用
    chucunId(state, data) {
      console.log(data, 'data过滤ID')
      state.lixian.map((item, value) => {
        item.data.map(item2 => {
          if (item2 === data) {
            console.log(item2, '匹配', value)
            // 单词本id供收藏功能使用
            sessionStorage.setItem('dcbsj', JSON.stringify(state.lixian[value]))
          }
        })
      })

    },
    // 单词释义编辑事件
    bianji(state, data) {
      console.log(data, '编辑')
      let index = state.lixian.findIndex(item => item.id == data[1])
      let index2 = state.lixian[index].data.findIndex(item => item.id == data[0].id)
      state.lixian[index].data[index2].dcfy = data[0].dcfy
      state.lixian[index].data[index2].shiyi = data[0].shiyi
      console.log(index, index2, data, '索引')
      //  如果收藏了,把收藏的数据也更新
      if (data[0].shouchang) {
        let index3 = state.wdsc.dc.findIndex(item => item.id == data[0].id)
        state.wdsc.dc[index3].dcfy = data[0].dcfy
        state.wdsc.dc[index3].shiyi = data[0].shiyi
      }
      state.bianjishujv = data[0]

    }
  },

  // actions专门提供异步操作
  actions: {

  },
  // getters对state里面的数据进行加工处理成新的数据
  getters: {
    // 处理学习计划的数据，供DanciBen.vue渲染页面   (该方法暂时弃用)
    /*   chuloixxjh(state){
        state.hqjs.forEach((item) => {
          console.log(item);
          let num = {
              id: item.id,
              dcs: item.zgdc,
              name: item.cydc,
              bt: item.cydc + "常见单词",
              img: item.img,
          };
          // this.shujv2.push(num);
          return num
      });
      } */
  },
  // 开启本地储存（开启后浏览器刷新数据不会消失）
  //  plugins: [createPersistedState()]
})