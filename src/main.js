import Vue from 'vue'
import App from './App.vue'
import 'vant/lib/index.css';
Vue.config.productionTip = false
import VueKeepScrollPosition from 'vue-keep-scroll-position'

Vue.use(VueKeepScrollPosition)
// 引入路由
import { router } from './router/index.js'
// 挂载Vuex
import store from "@/store.js"
// 局部导入Vant2

import {
  Dialog, Empty, Field, Form, Radio, RadioGroup, Cell, CellGroup, 
  SwipeCell, Card, Notify, Image as VanImage, Col, Row, Button, 
  Tab, Tabs, Toast, Search, Swipe,SwipeItem,Progress,Popup,DatetimePicker,
  NavBar, Icon, Tabbar, TabbarItem
} from 'vant';
Vue.use(Empty)
Vue.use(Dialog)
Vue.use(Form);
Vue.use(Field);
Vue.use(Radio)
Vue.use(RadioGroup)
Vue.use(Cell);
Vue.use(CellGroup);
Vue.use(Card);
Vue.use(SwipeCell);
Vue.use(Notify)
Vue.use(Col);
Vue.use(Row);
Vue.use(VanImage);
Vue.use(Button)
Vue.use(Tab);
Vue.use(Tabs);
Vue.use(Toast)
Vue.use(Search);
Vue.use(DatetimePicker);
Vue.use(Swipe);
Vue.use(SwipeItem);
Vue.use(Progress);
Vue.use(Popup);
Vue.use(NavBar);
Vue.use(Icon);
Vue.use(Tabbar);
Vue.use(TabbarItem);
new Vue({
  render: h => h(App),
  router,
  store
}).$mount('#app')
